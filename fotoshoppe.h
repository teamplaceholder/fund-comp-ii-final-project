/*********JMJ************
 * Filename: fotoshoppe.h
 * Name:  Paul Fortin
 * Final Project
 *
 * Description: interface for the driver class.
 ***********************/

#ifndef FOTOSHOPPE_H
#define FOTOSHOPPE_H

#include <QMainWindow>
#include "Pixel.h"
#include "Image.h"

namespace Ui {
class FotoShoppe;
}

class FotoShoppe : public QMainWindow
{
    Q_OBJECT

public:
    explicit FotoShoppe(QWidget *parent = 0);
    ~FotoShoppe();
    void display();
    void exitPreview();

private slots:
    void on_btnFlipVertical_clicked();

    void on_btnFlipHorizontal_clicked();

    void on_btnRotateCw_clicked();

    void on_btnRotateCCW_clicked();

    void on_chkNegateRed_clicked();

    void on_chkNegateBlue_clicked();

    void on_chkNegateGreen_clicked();

    void on_chkGray_clicked();

    void on_actionSave_triggered();

    void on_btnCrop_clicked();

    void on_spnCropDistance_valueChanged(const QString &arg1);

    void on_actionExit_triggered();

    void on_actionOpen_triggered();

private:
    Ui::FotoShoppe *ui;
    Image* imagePtr;
    Image* ungrayPtr;
    string inFileStr;
    QPixmap pmap;
    bool inPreview;
    /*used for crop preview and cropping*/
    int botVal;
    int topVal;
    int leftVal;
    int rightVal;
};

#endif // FOTOSHOPPE_H
