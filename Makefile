GXX :=g++
GXXFLAGS :=-c
OBJECTS :=Pixel.o main.o Image.o
OBJ1 :=Pixel
OBJ2 :=main
OBJ3 :=Image
EXECUTABLE :=image

$(EXECUTABLE): $(OBJECTS)
	$(GXX) $(OBJECTS) -o $(EXECUTABLE)

$(OBJ1).o: $(OBJ1).cpp
	$(GXX) $(GXXFLAGS) $(OBJ1).cpp 

$(OBJ2).o: $(OBJ2).cpp
	$(GXX) $(GXXFLAGS) $(OBJ2).cpp

$(OBJ3).o: $(OBJ3).cpp
	$(GXX) $(GXXFLAGS) $(OBJ3).cpp

clean: 
	rm -f *.o $(EXECUTABLE)
