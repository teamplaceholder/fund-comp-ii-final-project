#-------------------------------------------------
#
# Project created by QtCreator 2013-04-24T21:31:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AbodeFotoShoppe
TEMPLATE = app


SOURCES += main.cpp\
        fotoshoppe.cpp \
    Image.cpp \
    Pixel.cpp

HEADERS  += fotoshoppe.h \
    Image.h \
    Pixel.h

FORMS    += fotoshoppe.ui
