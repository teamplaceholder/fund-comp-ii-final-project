/*********JMJ************
 * Filename: Image.cpp
 * Name:  Paul Fortin
 * Final Project
 *
 * Description: Implementation for the Image class, a composition of Pixels
 ***********************/

#include<iostream>
#include<vector>
#include<deque>
#include<string>
#include<fstream>
#include<stdlib.h>
#include<algorithm>
#include"Pixel.h"
#include"Image.h"

using namespace std;


/*constructor*/
Image::Image(string filename){
    ifstream original(filename.c_str()); //image to be modified
/*store the output from the filestream, and convert to ints later*/
    string colString;
    string rowString;
    string colorString;
    string rString;
    string gString;
    string bString;
/*Parse header*/
    if(original.is_open()){
        /*get magicNumber*/
        original>>MagicNumber;
        /*get columns*/
        original >> colString;
        cols = atoi(colString.c_str());
        /*get rows*/
        original >> rowString;
        rows = atoi(rowString.c_str());
        /*get max Color*/
        original >> colorString;
        color = atoi(colorString.c_str());
    /*Read each row, get the RGB value for each column*/
        for(int i=0; i<rows; i++){
            deque< Pixel > row; //store a row of pixels
            for(int j=0; j<cols; j++){
                original >> rString;
                original >> gString;
                original >> bString;
                //cout<<rString<<" "<<gString<<" "<<bString<<" ";
                int r = atoi(rString.c_str());
                int g = atoi(gString.c_str());
                int b = atoi(bString.c_str());
                Pixel pix(r, g, b);
                row.push_back(pix);
            }
            //cout<<endl;
            pic.push_back(row);
        }
    }
}


/*******************
 * Name: Image.print
 * Purpose: display the data of the pic vector
 * @input none
 * @return none
 ******************/
void Image::print(){

    cout<<MagicNumber<<endl;
    cout<<cols<<"\t"<<rows<<endl;
    cout<<color<<endl;
    for(int i=0; i<rows; i++){
        for(int j=0; j<cols; j++){
            cout<<pic[i][j];
        }
        cout<<endl;
    }

}

/*******************
 * Name: Image.getCols
 * Purpose: return the number of cols
 * @input none
 * @return an integer indicating the number of columns
 ******************/
int Image::getCols(){
    return cols;
}

/*******************
 * Name: Image.getRows
 * Purpose: return the number of rows
 * @input none
 * @return an integer indicating the number of rows
 ******************/
int Image::getRows(){
    return rows;
}

/*******************
 * Name: Image.negateR
 * Purpose: negate red values across the image
 * @input none
 * @return none
 ******************/
void Image::negateR(){
    for(int i=0; i<rows; i++){
        for(int j=0; j<cols; j++){
            pic[i][j].negateRed(color);
        }
    }
}

/*******************
 * Name: Image.negateG
 * Purpose: negate green values across the image
 * @input none
 * @return none
 ******************/
void Image::negateG(){
    for(int i=0; i<rows; i++){
        for(int j=0; j<cols; j++){
            pic[i][j].negateGreen(color);
        }
    }
}

/*******************
 * Name: Image.negateB
 * Purpose: negate blue values across the image
 * @input none
 * @return none
 ******************/
void Image::negateB(){
    for(int i=0; i<rows; i++){
        for(int j=0; j<cols; j++){
            pic[i][j].negateBlue(color);
        }
    }
}

/*******************
 * Name: Image.grayscale
 * Purpose: grayscale the entire image
 * @input none
 * @return none
 ******************/
void Image::grayscale(){
    for(int i=0; i<rows; i++){
        for(int j=0; j<cols; j++){
            pic[i][j].gray();
        }
    }
}

/*******************
 * Name: Image.FlipHorizontal
 * Purpose: flip the image over the y axis
 * @input none
 * @return none
 ******************/
void Image::FlipHorizontal(){
    /*for each row, swap the last place with the first, increment the front, decrement the back, swap, repeat.*/
    int sloc;//backwards iterator.
    for(int i=0; i<rows; i++){
        for(int j=0; j<cols/2; j++){//j is forward iterator
            sloc=cols-j-1;//-1 because index starts at 0;
            swap(pic[i][j], pic[i][sloc]); //swap exchanges two elements in an array
        }
    }
}

/*******************
 * Name: Image.FlipVertical
 * Purpose: flip image over the x axis
 * @input none
 * @return none
 ******************/
void Image::FlipVertical(){
    int swor; //backwards iterator
    for(int i=0; i<rows/2; i++){ //i is forward iterator
        swor = rows-i-1;//index starts at 0;
        swap(pic[i], pic[swor]);
    }
}

/*******************
 * Name: Image.transpose
 * Purpose: helper function to transpose the matrix, as part of the algorithm for rotation
 * @input none
 * @return none
 ******************/
void Image::transpose(){

    /*add rows or columns to the matrix to make it square*/
    int diff=0;	//how many rows/columns to add on to make matrix square.
    if(rows>cols){
         diff=rows-cols;
        for(int i=0; i<rows; i++){ //add empty columns on to make square
            for(int k=0; k<diff; k++){
                Pixel temp(0, 0, 0);
                pic[i].push_back(temp);
            }
        }
    }else if(cols>rows){
         diff=cols-rows;
        for(int k=0; k<diff; k++){	//how many rows to add.
            deque< Pixel > row;
            for(int j=0; j<cols; j++){//populate a row of 0's
                Pixel temp(0, 0, 0);
                row.push_back(temp);
            }
            pic.push_back(row);
        }
    }

    /*swap the elements of the row index and column index.  eg 1,2 -> 2,1*/
    /*need two different cases because you need to make sure that the swaps reach all of the elements in the matrix.  Therefore, outer loop is the smaller value. */
    if(rows<=cols){
        for(int i=0; i<rows; i++){
            for(int j=i; j<cols; j++){
                swap(pic[i][j], pic[j][i]);
            }
        }
    }else if(cols<rows){
        for(int j=0; j<cols; j++){
            for(int i=j; i<rows; i++){
                swap(pic[i][j], pic[j][i]);
            }
        }
    }
/*re-assign the number of rows and columns in the image
If the matrix is square, this will not change, otherwise the transpose will interchange the number of row and columns in the matrix*/
    int temp;
    temp=rows;
    rows=cols;
    cols=temp;

/*remove zeros that were added earlier*/
    if(cols>rows){//zeros are at the end of the rows
        for(int k=0; k<diff; k++){
            pic.pop_back();
        }
    }else if(rows>cols){//zeros are at the end of the cols
        for(int i=0; i<rows; i++){
            for(int k=0; k<diff; k++){
                pic[i].pop_back();
            }
        }
    }
}

/*The algorithm for a rotation is to transpose the matrix, and then flip horizontal for Clockwise, or flip vertical for Counter-clockwise.*/

/*******************
 * Name: Image.RotateCW
 * Purpose: rotate the image clockwise using alg. above
 * @input none
 * @return none
 ******************/
void Image::RotateCW(){
    transpose();
    FlipHorizontal();
}

/*******************
 * Name: Image.RotateCCW
 * Purpose: rotate image counter clockwise, using alg. above
 * @input none
 * @return none
 ******************/
void Image::RotateCCW(){
    transpose();
    FlipVertical();
}

/*******************
 * Name: Image.write
 * Purpose: output the 2d deque to a file
 * @input a string indicating the name of the file to be written to
 * @return none
 ******************/
void Image::write(string filename){
    ofstream output(filename.c_str());
    if(output.is_open()){
        output<<MagicNumber<<endl;
        output<<cols<<"\t"<<rows<<endl;
        output<<color<<endl;
        for(int i=0; i<rows; i++){
            for(int j=0; j<cols; j++){
                output<<pic[i][j];
            }
            output<<endl;
        }
    }
}

/*******************
 * Name: Image.cropTop
 * Purpose: remove num rows from the front of the outer deque
 * @input an integer indicating then number of rows to be removed
 * @return none
 ******************/
void Image::cropTop(int num){
    for(int i=0; i<num; i++){
        pic.pop_front();
    }
    rows -= num;
}

/*******************
 * Name: Image.cropLeft
 * Purpose: remove num cols from the front of the inner deque
 * @input an integer indicating then number of cols to be removed
 * @return none
 ******************/
void Image::cropLeft(int num){
    for(int r=0; r<rows; r++){
        for(int i=0; i<num; i++){
            pic[r].pop_front();
        }
    }
    cols-=num;
}

/*******************
 * Name: Image.cropBot
 * Purpose: remove num rows from the back of the outer deque
 * @input an integer indicating then number of rows to be removed
 * @return none
 ******************/
void Image::cropBot(int num){
    for(int i=0; i<num; i++){
        pic.pop_back();
    }
    rows -= num;
}

/*******************
 * Name: Image.cropRight
 * Purpose: remove num cols from the back of the inner deque
 * @input an integer indicating then number of cols to be removed
 * @return none
 ******************/
void Image::cropRight(int num){
    for(int r=0; r<rows; r++){
        for(int i=0; i<num; i++){
            pic[r].pop_back();
        }
    }
    cols -= num;
}

/*******************
 * Name: Image.blackRight
 * Purpose: set all the pixels num columns from the right to zero
 * @input an integer indicating then number of cols to be blacked out
 * @return none
 ******************/
void Image::blackRight(int num){
    for(int r=0; r<rows; r++){
        for(int i=0; i<num; i++){
            pic[r][cols-1-i].black();
        }
    }
}

/*******************
 * Name: Image.blackLeft
 * Purpose: set all the pixels num columns from the left to zero
 * @input an integer indicating then number of cols to be blacked out
 * @return none
 ******************/
void Image::blackLeft(int num){
    for(int r=0; r<rows; r++){
        for(int i=0; i<num; i++){
            pic[r][i].black();
        }
    }
}

/*******************
 * Name: Image.blackTop
 * Purpose: set all the pixels num rows from the top to zero
 * @input an integer indicating then number of rows to be blacked out
 * @return none
 ******************/
void Image::blackTop(int num){
    for(int i=0; i<num; i++){
        for(int col=0; col<cols; col++){
            pic[i][col].black();
        }
    }
}

/*******************
 * Name: Image.blackBot
 * Purpose: set all the pixels num rows from the bottom to zero
 * @input an integer indicating then number of rows to be blacked out
 * @return none
 ******************/
void Image::blackBot(int num){
    for(int i=0; i<num; i++){
        for(int col=0; col<cols; col++){
            pic[rows-1-i][col].black();
        }
    }
}
