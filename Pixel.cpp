/*********JMJ************
 * Filename: Pixel.cpp
 * Name:  Paul Fortin
 * Final Project
 * 
 * Description: Implementation for the Pixel class, which will be composed into an
 * 								image
 ***********************/

#include<iostream>
#include<math.h>
#include"Pixel.h"

using namespace std;

/*constructor*/
Pixel::Pixel(int red, int green, int blue){
/*set initial values for the RGB.*/
	r=red;
	g=green;
	b=blue;

}

/*******************
 * Name: <<
 * Purpose: overload operator to display RGB
 * @input the ostream output(for cascading) and a Pixel object
 * @return the output of the ostream
 ******************/
ostream &operator<< (ostream &output, const Pixel &pix){
	output<<pix.r<<" "<<pix.g<<" "<<pix.b<<" ";
	return output;
}

/*******************
 * Name: Pixel.getr
 * Purpose: return r value
 * @input none
 * @return integer indicating the r value of the pixel
 ******************/
int Pixel::getr(){
	return r;
}

/*******************
 * Name: Pixel.getg
 * Purpose: return g value
 * @input none
 * @return integer indicating the g value of the pixel
 ******************/
int Pixel::getg(){
	return g;
}

/*******************
 * Name: Pixel.getb
 * Purpose: return b value
 * @input none
 * @return integer indicating the b value of the pixel
 ******************/
int Pixel::getb(){
	return b;
}

/*******************
 * Name: Pixel.negateRed
 * Purpose: negate red value
 * @input an integer with the highes possible color value
 * @return none
 ******************/
void Pixel::negateRed(int color){
	r=fabs(color-r);
}

/*******************
 * Name: Pixel.negateGreen
 * Purpose: negate green value
 * @input an integer with the highes possible color value
 * @return none
 ******************/
void Pixel::negateGreen(int color){
	g=fabs(color-g);
}

/*******************
 * Name: Pixel.negateBlue
 * Purpose: negate blue value
 * @input an integer with the highes possible color value
 * @return none
 ******************/
void Pixel::negateBlue(int color){
	b=fabs(color-b);
}

/*******************
 * Name: Pixel.gray
 * Purpose: average out all the colors of the pixel, set all to average
 * @input none
 * @return none
 ******************/
void Pixel::gray(){
	int average = (r+g+b)/3;
	r=average;
	g=average;
	b=average;
}

void Pixel::black(){
    r=0;
    g=0;
    b=0;
}
