/*********JMJ************
 * Filename: Image.h
 * Name:  Paul Fortin
 * Final Project
 *
 * Description: Interface for the Image class, a composition of Pixels
 ***********************/

#ifndef IMAGE_H
#define IMAGE_H

#include<iostream>
#include"Pixel.h"
#include<vector>
#include<deque>
#include<string>

using namespace std;

class Image {

    public:
        Image(string);//constructor, takes filename as argument
        void print(); //print out the data of the image read in.
        void negateR(); //negative effect on R.
        void negateG(); //negative effect on G.
        void negateB(); //negative effect on B.
        void grayscale();//grayscale the image
        int getCols();//return the number of columns.
        int getRows();//return the number of rows
        void FlipHorizontal(); //flip the image over the y axis.
        void FlipVertical(); //flip the image over the x axis.
        void RotateCW();//transpose matrix, then horizontal flip.
        void RotateCCW();//transpose matrix, then vertical flip
        void cropTop(int);//remove int rows from the top.
        void cropLeft(int);//remove int cols from the left.
        void cropBot(int);//remove int rows from the bottom.
        void cropRight(int);//remove int cols from the right.
        void write(string);//output modified image matrix to a file
        void blackRight(int);//zero out all the pixels on the right side of the pic
        void blackLeft(int);//zero out all the pixels on the left side of the pic
        void blackTop(int);//zero out all the pixels on the top of the pic
        void blackBot(int);//zero out all the pixels on hte bottom of the pic
    private:
        void transpose(); //transpose the matrix, used in rotation.
        deque< deque <Pixel> > pic; //2d vector used to store the Pixels of the image
        int rows; //number of rows in the picture, read in from header
        int cols; //number of columns in the picture, read in from header
        int color; //maximum value of the color, read in from header
        string MagicNumber; //indicates the type of data used in the file.
                                        /* P3(decimal RGB) for now, later use P6(binary RGB)*/
};

#endif
