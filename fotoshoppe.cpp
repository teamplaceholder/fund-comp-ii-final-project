/*********JMJ************
 * Filename: fotoshoppe.cpp
 * Name:  Paul Fortin
 * Final Project
 *
 * Description: Implementation of the fotoshoppe class, which is the driver for the program.
 ***********************/

#include "fotoshoppe.h"
#include "ui_fotoshoppe.h"
#include <QtWidgets>
#include <QImage>
#include "Pixel.h"
#include "Image.h"

/*Constructor*/
FotoShoppe::FotoShoppe(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FotoShoppe)
{
    ui->setupUi(this);

   QString inputFile( QFileDialog::getOpenFileName(this,
                      tr("Open File"), QDir::currentPath()));
   inFileStr = inputFile.toStdString();

   imagePtr = new Image(inFileStr);
   ungrayPtr = imagePtr;
   inPreview = false;
   imagePtr->write("temp.ppm"); //create the working image file

   ui->imageLabel->setPixmap(QPixmap(inputFile));
   ui->imageLabel->setScaledContents(1);
   ui->imageLabel->show();

}

/*deconstructor*/
FotoShoppe::~FotoShoppe()
{
    delete ui;
    delete imagePtr;

}

/*******************
 * Name: fotoshoppe.display()
 * Purpose: show the current image in the label
 * @input none
 * @return none
 ******************/
void FotoShoppe::display(){
    imagePtr->write("temp.ppm");
    QString inputFile("temp.ppm");
    ui->imageLabel->setPixmap(QPixmap(inputFile));
    ui->imageLabel->setScaledContents(1);
    ui->imageLabel->show();
}

/*******************
 * Name: Image.on_btnFlipVertical_clicked
 * Purpose: handle when button FlipVertical is clicked
 * @input none
 * @return none
 ******************/
void FotoShoppe::on_btnFlipVertical_clicked()
{
    exitPreview();
    imagePtr->FlipVertical();
    if(ui->chkGray->isChecked()){
        ungrayPtr->FlipVertical();
    }
    display();

}

/*******************
 * Name: Image.on_btnFlipHorizontal_clicked
 * Purpose: handle when button FlipHorizontal is clicked
 * @input none
 * @return none
 ******************/
void FotoShoppe::on_btnFlipHorizontal_clicked()
{
    exitPreview();
    imagePtr->FlipHorizontal();
    if(ui->chkGray->isChecked()){
        ungrayPtr->FlipHorizontal();
    }
    display();
}

/*******************
 * Name: Image.on_btnRotateCw_clicked
 * Purpose: handle when button RotateCw is clicked
 * @input none
 * @return none
 ******************/
void FotoShoppe::on_btnRotateCw_clicked()
{
    exitPreview();
    imagePtr->RotateCW();
    if(ui->chkGray->isChecked()){
        ungrayPtr->RotateCW();
    }
    display();
}

/*******************
 * Name: Image.on_btnFlipHorizontal_clicked
 * Purpose: handle when button FlipHorizontal is clicked
 * @input none
 * @return none
 ******************/
void FotoShoppe::on_btnRotateCCW_clicked()
{
    exitPreview();
    imagePtr->RotateCCW();
    if(ui->chkGray->isChecked()){
        ungrayPtr->RotateCCW();
    }
    display();
}

/*******************
 * Name: Image.on_btnNegateRed_clicked
 * Purpose: handle when button NegateRed is clicked
 * @input none
 * @return none
 ******************/
void FotoShoppe::on_chkNegateRed_clicked()
{
    exitPreview();
    /*make the negation and the grayscale mutually exclusive*/
    if(ui->chkGray->isChecked()){
        ui->chkGray->setChecked(0);
        imagePtr = ungrayPtr;
        imagePtr->write("temp.ppm");
    }
    imagePtr->negateR();
    display();
}

/*******************
 * Name: Image.on_btnNegateBlue_clicked
 * Purpose: handle when button NegateBlue is clicked
 * @input none
 * @return none
 ******************/
void FotoShoppe::on_chkNegateBlue_clicked()
{
    exitPreview();
    if(ui->chkGray->isChecked()){
        ui->chkGray->setChecked(0);
        imagePtr = ungrayPtr;
        imagePtr->write("temp.ppm");
    }
    imagePtr->negateB();
    display();
}

/*******************
 * Name: Image.on_btnNegateGreen_clicked
 * Purpose: handle when button NegateGreen is clicked
 * @input none
 * @return none
 ******************/
void FotoShoppe::on_chkNegateGreen_clicked()
{
    exitPreview();
    if(ui->chkGray->isChecked()){
        ui->chkGray->setChecked(0);
        imagePtr = ungrayPtr;
        imagePtr->write("temp.ppm");
    }
    imagePtr->negateG();
    display();
}

/*******************
 * Name: Image.on_chkGray_clicked
 * Purpose: handle when checkbox Gray is clicked
 * @input none
 * @return none
 ******************/
void FotoShoppe::on_chkGray_clicked()
{
    exitPreview();
    /*make negation and grayscale mutually exclusive*/
    if(ui->chkNegateBlue->isChecked()){
        ui->chkNegateBlue->setChecked(0);
        imagePtr->negateB();
        imagePtr->write("temp.ppm");
    }
    if(ui->chkNegateGreen->isChecked()){
        ui->chkNegateGreen->setChecked(0);
        imagePtr->negateG();
        imagePtr->write("temp.ppm");
    }
    if(ui->chkNegateRed->isChecked()){
        ui->chkNegateRed->setChecked(0);
        imagePtr->negateR();
        imagePtr->write("temp.ppm");
    }

    if(ui->chkGray->isChecked()){
        ungrayPtr = new Image("temp.ppm");
        imagePtr->grayscale();
        display();
    }else{
        imagePtr = ungrayPtr;
        display();
    }
}

/*******************
 * Name: Image.on_actionSave_triggered
 * Purpose: handle when menu option Save is clicked
 * @input none
 * @return none
 ******************/
void FotoShoppe::on_actionSave_triggered()
{
    exitPreview();
    QString saveFile( QFileDialog::getSaveFileName(this,
                            tr("Save File"), QString()));
    string saveFileStr = saveFile.toStdString();
    if(!saveFile.isEmpty()){
       imagePtr->write(saveFileStr);
    }

}

/*******************
 * Name: Image.on_btnCrop_clicked
 * Purpose: handle when button Crop is clicked
 * @input none
 * @return none
 ******************/
void FotoShoppe::on_btnCrop_clicked()
{
        /*prevent user from entering a value higher than the remaining rows*/
        if((botVal+topVal) > imagePtr->getRows()){
              ui->radCropTop->setChecked(true);
              botVal=0;
              ui->spnCropDistance->setValue(imagePtr->getRows());
        }

        /*prevent the user from entering a value higher than the remaining columns*/
        if((leftVal + rightVal) > imagePtr->getCols()){
             ui->radCropRight->setChecked(true);
             leftVal=0;
             ui->spnCropDistance->setValue(imagePtr->getCols());
        }

        /*crop the image and display*/
        imagePtr->cropBot(botVal);
        imagePtr->cropTop(topVal);
        imagePtr->cropLeft(leftVal);
        imagePtr->cropRight(rightVal);

        /*reset the cropping variables and the spin counter*/
        botVal=0;
        topVal=0;
        leftVal=0;
        rightVal=0;
        display();
        inPreview = false;
        ui->spnCropDistance->setValue(0);

}

/*******************
 * Name: Image.on_spnCropDistance_valueChanged
 * Purpose: set the cropping distance for each side based on spin counter
 * @input none(arg1 is not used)
 * @return none
 ******************/
void FotoShoppe::on_spnCropDistance_valueChanged(const QString &arg1)
{
    if(!inPreview){
        imagePtr->write("preview.ppm");
        inPreview = true;
    }

    if(ui->radCropBottom->isChecked()){
        botVal=ui->spnCropDistance->value();
    }else if(ui->radCropTop->isChecked()){
        topVal=ui->spnCropDistance->value();
    }else if(ui->radCropLeft->isChecked()){
        leftVal=ui->spnCropDistance->value();
    }else if(ui->radCropRight->isChecked()){
        rightVal=ui->spnCropDistance->value();
    }

    /*prevent user from entering a value higher than the remaining rows*/
    if((botVal+topVal) > imagePtr->getRows()){
          ui->radCropTop->setChecked(true);
          botVal=0;
          ui->spnCropDistance->setValue(imagePtr->getRows());
    }

    /*prevent the user from entering a value higher than the remaining columns*/
    if((leftVal + rightVal) > imagePtr->getCols()){
         ui->radCropRight->setChecked(true);
         leftVal=0;
         ui->spnCropDistance->setValue(imagePtr->getCols());
    }
    delete imagePtr;
    imagePtr = new Image("preview.ppm");
    imagePtr->blackBot(botVal);
    imagePtr->blackLeft(leftVal);
    imagePtr->blackRight(rightVal);
    imagePtr->blackTop(topVal);
    display();
}

/*******************
 * Name: Image.on_actionExit_triggered
 * Purpose: handle when menu option Exit is clicked
 * @input none
 * @return none
 ******************/
void FotoShoppe::on_actionExit_triggered()
{
    this->close();
}

/*******************
 * Name: Image.on_actionOpen_triggered
 * Purpose: handle when menu option Open is clicked
 * @input none
 * @return none
 ******************/
void FotoShoppe::on_actionOpen_triggered()
{
    exitPreview();
    QString inputFile( QFileDialog::getOpenFileName(this,
                       tr("Open File"), QDir::currentPath()));
    inFileStr = inputFile.toStdString();
    delete imagePtr;
    /*reset checkboxes*/
    ui->chkGray->setChecked(0);
    ui->chkNegateBlue->setChecked(0);
    ui->chkNegateGreen->setChecked(0);
    ui->chkNegateRed->setChecked(0);
    ui->spnCropDistance->setValue(0);
    botVal=0;
    rightVal=0;
    leftVal=0;
    topVal=0;

    imagePtr = new Image(inFileStr);
    imagePtr->write("temp.ppm"); //create the working image file

    ui->imageLabel->setPixmap(QPixmap(inputFile));
    ui->imageLabel->setScaledContents(1);
    ui->imageLabel->show();
}

/*******************
 * Name: exitPreview
 * Purpose: check if the image is in crop preview mode, and restore it if it is
 * @input none
 * @return none
 ******************/
void FotoShoppe::exitPreview(){
    if(inPreview){
        imagePtr=new Image("preview.ppm");
        inPreview = false;
    }
}
