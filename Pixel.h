/*********JMJ************
 * Filename: Pixel.h
 * Name:  Paul Fortin
 * Final Project
 * 
 * Description: Interface for the Pixel class, which will be composed into an image
 ***********************/


/****Functions to modify RBG will be added later****/
#ifndef PIXEL_H
#define PIXEL_H

#include<iostream>

using namespace std;

class Pixel {
	friend ostream &operator<< (ostream &, const Pixel &); //overload << operator
	
	public:
		Pixel(int=0, int=0, int=0); //constructor, default each value to 0
		int getr();//return r value
		int getg();//return g value
		int getb();//return b value
        void black();//set pixel to black;
		void negateRed(int);//negative effect on red
		void negateGreen(int);//negative effect on green
		void negateBlue(int);//negative effect on blue
		void gray(); //average out all of the elements of the pixel
	private:
		int r;//red value
		int g;//green value
		int b;//blue value 

};

#endif
